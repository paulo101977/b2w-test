// Importar bibliotecas do react.
import React from 'react'
import { storiesOf } from '@storybook/react'

import { ThemeProvider } from 'styled-components'
import Purple from '../../config/themes/Purple'

import Grid from './../Grid'
import Title from './../Title'
import Text from './../Text'
import Icon from '.'

storiesOf('Atoms/Icon', module)

//adiciona um subitem no storybook.
.add('Info', () => (
    <ThemeProvider theme={ Purple }>
        <Grid container>
            <Grid row margin={[50, 0]}>
                <Grid col sizes={[12, 12]}>
                    <Title type={1} themeColor='primary' >Icon</Title>
                    <Text type='p'>Iconografia.</Text>
                </Grid>
            </Grid>
            <Grid row>
                <Grid col sizes={[1, 12]}>
                <Text type='p' themeColor="error">Inserir aqui tabela de props</Text>
                </Grid>

            </Grid>
        </Grid>
    </ThemeProvider>
))

.add('Use Case', () => (
    <ThemeProvider theme={ Purple }>
    <Grid className="container">
        <Grid className="row">
            <Grid margin={[20,0, 0]} className="col-12">
                <Title type={3} themeColor='primary' >ThemeColor</Title>
                <Text>Cores aplicadas direto do tema.</Text>
            </Grid>
        </Grid>
        <Grid className="row">
            <Grid className="col-12" margin={[20, 0]}>
                <Icon type="oi" size={3} themeColor="primary" />
                <Icon type="oi" size={3} themeColor="secondary" />
                <Icon type="oi" size={3} themeColor="success" />
                <Icon type="oi" size={3} themeColor="error" />
                <Icon type="oi" size={3} themeColor="warning" />
                <Icon type="oi" size={3} themeColor="info" />
                <Icon type="oi" size={3} themeColor="dark" />
                <Icon type="oi" size={3} themeColor="light" />
                <Icon type="oi" size={3} themeColor="link" />
            </Grid>
        </Grid>
        <Grid className="row">
            <Grid className="col-12" themeColor="light">
                <pre>
                    <code>
{`<Icon type="{icon_name}" themeColor="primary" />
<Icon type="{icon_name}" themeColor="secondary" />
<Icon type="{icon_name}" themeColor="success" />
<Icon type="{icon_name}" themeColor="error" />
<Icon type="{icon_name}" themeColor="warning" />
<Icon type="{icon_name}" themeColor="info" />
<Icon type="{icon_name}" themeColor="dark" />
<Icon type="{icon_name}" themeColor="light" />
<Icon type="{icon_name}" themeColor="link" />`}
                    </code>
                </pre>
            </Grid>
        </Grid>
        <Grid className="row">
            <Grid className="col-12" margin={[20,0, 0]}>
                <Title type={3} themeColor='primary' >type</Title>
                <Text>Biblioteca de ícones disponíveis.</Text>
            </Grid>
        </Grid>
        <Grid className="row">
            <Grid className="col-12" margin={[20, 0]}>
                <Icon type="aprender" size={3} />
                <Icon type="arrowdown" size={3} />
                <Icon type="c2c" size={3} />
                <Icon type="caret" size={3} />
                <Icon type="celular" size={3} />
                <Icon type="chat" size={3} />
                <Icon type="check" size={3} />
                <Icon type="close" size={3} />
                <Icon type="combo" size={3} />
                <Icon type="conexao" size={3} />
                <Icon type="desconto" size={3} />
                <Icon type="facebook" size={3} />
                <Icon type="feat_antivirus" size={3} />
                <Icon type="feat_certificate" size={3} />
                <Icon type="feat_cloud" size={3} />
                <Icon type="feat_modem" size={3} />
                <Icon type="feat_wifi" size={3} />
                <Icon type="google_plus" size={3} />
                <Icon type="internet" size={3} />
                <Icon type="lupa_thin" size={3} />
                <Icon type="oi" size={3} />
                <Icon type="prod_bandalarga" size={3} />
                <Icon type="prod_fixo" size={3} />
                <Icon type="prod_pos" size={3} />
                <Icon type="prod_tvhd" size={3} />
                <Icon type="prod_tv" size={3} />
                <Icon type="protecao" size={3} />
                <Icon type="recompensa" size={3} />
                <Icon type="seguranca" size={3} />
                <Icon type="suporte" size={3} />
                <Icon type="twitter" size={3} />
                <Icon type="youtube" size={3} />
            </Grid>
        </Grid>
        <Grid className="row">
            <Grid className="col-12" themeColor="light">
                <pre>
                    <code>
{`<Icon type="{icon_name}" />`}
                    </code>
                </pre>
            </Grid>
        </Grid>
        <Grid className="row">
            <Grid className="col-12" margin={[20,0, 0]}>
                <Title type={3} themeColor='primary' >Size</Title>
                <Text>Todos os tamanhos de ícones disponíveis.</Text>
            </Grid>
        </Grid>
        <Grid className="row">
        <Grid className="col-12" margin={[20, 0]}>
                <Icon type="oi" size={-2} />
                <Icon type="oi" size={-1} />
                <Icon type="oi" size={1} />
                <Icon type="oi" size={2} />
                <Icon type="oi" size={3} />
                <Icon type="oi" size={4} />
                <Icon type="oi" size={5} />
                <Icon type="oi" size={6} />

            </Grid>

        </Grid>
        <Grid className="row">
            <Grid className="col-12" themeColor="light">
                <pre>
                    <code>
{`<Icon size="{icon_name}" />`}
                    </code>
                </pre>
            </Grid>
        </Grid>
    </Grid>
</ThemeProvider>
))
