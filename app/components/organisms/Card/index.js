import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { prop, ifProp, switchProp } from 'styled-tools'
import Link from './../../atoms/Link'
import Button from './../../atoms/Button'
import List from './../../atoms/List'
import Text from './../../atoms/Text'
import Title from './../../atoms/Title'
import Icon from './../../atoms/Icon'
import {Grid} from '../../atoms/Grid'
import {Glyph} from '../../atoms/Glyph'

//Component Styles
const CardContainer = styled.div`
  box-shadow:
    0 4px 10px 0 rgba(0,0,0,0.2),
    0 4px 20px 0 rgba(0,0,0,0.19);
  margin: 1rem;
  display: flex;
  flex-direction: column;
  padding: 0;
  align-items: center;
  transition: transform 300ms;
  background-color: white;

  :hover{
    transform: scale(1.1);
  }

  .header{
    display: flex;
    flex-direction: row;
    width: 100%;
    padding: 1rem;
    background-color: ${props => props.theme.colors.primary};

    /* ensure the centered align*/
    * {
      display: block;
      width: auto;
    }

    .swg{
      margin-top: 0.5rem;
      margin-right: 1rem;
    }
  }

  .content{
    width: 100%;
    padding: 1rem;

    .info-text{
        font-weight: bolder;
        margin-right: 0.5rem;
    }

    .swg{
      margin-right: 2rem;
    }
  }

  .featured{
    margin: 1rem 0;
    display: block !important;
    text-align: center;
    font-weight: bolder;
  }

  @media only screen and (max-width: 768px) {
    max-width: none;
  }
`

//Component Props
class CardElement extends Component {

    render() {

        const {
          name,
          population,
          climate,
          terrain,
          films
        } = this.props;

        return(
          <CardContainer
            className='card-container'>
              <Grid
                className="header"
                centerItems>
                <Glyph
                  themeColor="secundary"
                  className="swg swg-sw-alt swg-3x"/>
                <Text
                  fontSize={1}
                  margin={0}
                  gradient={{
                    direction: '135deg',
                    colors:  ['black', 'white']
                  }}>
                  {/* default title */}
                  {name}
                </Text>
              </Grid>
              <Grid
                col
                className="content"
                margin={[20, 0]}>
                <Grid centerItems>
                  <Glyph
                    themeColor="primary"
                    className="swg swg-quigonjinn swg-2x"/>
                  <span className="info-text">Population:</span>
                  <span>
                    {population}
                  </span>
                </Grid>
                <Grid centerItems>
                  <Glyph
                    themeColor="primary"
                    className="swg swg-blacksun swg-2x"/>
                  <span className="info-text">Climate:</span>
                  <span>
                    {climate}
                  </span>
                </Grid>
                <Grid centerItems>
                  <Glyph
                    themeColor="primary"
                    className="swg swg-deathstar-3  swg-2x"/>
                  <span className="info-text">Terrain:</span>
                  <span>
                    {terrain}
                  </span>
                </Grid>
              </Grid>
              <Grid className="featured" centerItems>
                Featured in {films.length}
              </Grid>
          </CardContainer>
        )
    }

}

const Card = (props) => <CardElement {...props} />

Card.PropTypes = {
    name: React.PropTypes.string.isRequired,
    population: React.PropTypes.number.isRequired,
    climate: React.PropTypes.string.isRequired,
    terrain: React.PropTypes.string.isRequired,
    films: React.PropTypes.array.isRequired
  }

Card.defaultProps = {
  name: 'Alderaan',
  population: 2000,
  climate: "Arid",
  terrain: "Desert",
  films: []
}

export {Card}
export default Card
