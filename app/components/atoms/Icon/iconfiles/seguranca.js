import React from 'react'

const seguranca = function(props){

  

  return (
    <svg
                viewBox="0 0 31 39"
                xmlns="http://www.w3.org/2000/svg"
            >
                <g
                    id="seguranca01" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <g id="1.1-pagina_sva-celular-v2" transform="translate(-218.000000, -1595.000000)" strokeWidth="1.21" >
                        <g id="segurança" transform="translate(98.000000, 1574.000000)">
                            <g id="icon-segurança" transform="translate(120.000000, 21.000000)">
                                <polygon
                                    id="segurança-1"
                                    points="0.62 38.390625 30.38372 38.390625 30.38372 9.126 0.62 9.126"
                                    stroke="20px"

                                >
                                </polygon>
                                <path
                                    d="M19.57712,21.6768094 C19.57712,23.8864031 17.75184,25.6804031 15.50248,25.6804031 C13.25188,25.6804031 11.4266,23.8864031 11.4266,21.6768094 C11.4266,19.4672156 13.25188,17.6719969 15.50248,17.6719969 C17.75184,17.6719969 19.57712,19.4672156 19.57712,21.6768094 Z"
                                    id="Stroke-3"
                                    stroke="20px"

                                >
                                </path>
                                <path
                                    d="M6.834508,9.12636563 C6.834508,4.42442812 10.716948,0.609740625 15.503348,0.609740625 C20.289748,0.609740625 24.169708,4.42442812 24.169708,9.12636563"
                                    id="Stroke-4"
                                    stroke="20px"
                                >
                                </path>
                                <path
                                    d="M15.502356,25.6806469 L15.502356,31.0650844"
                                    id="Stroke-5"
                                    stroke="20px"
                                >
                                </path>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
  )
}

export {seguranca}
export default seguranca