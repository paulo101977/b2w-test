import React from 'react'
import { Provider } from 'react-redux'
import { Router, Route, IndexRoute } from 'react-router'
import configureStore from 'store/configureStore'


import App from 'containers/App'

//import Faq from 'containers/faq'
//import Detail from 'containers/detail'
//import Test from 'containers/test'
import Main from 'containers/Main'


export default function(history) {
  return (
    <Router history={history}>
        <Route path="/" component={Main}>
        {/* <Route path="/oi-futuro" component={Detail} /> */}
        <IndexRoute component={Main} />
        </Route>
    </Router>
  )
}
