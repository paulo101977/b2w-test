import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as actions from '../actions/questions'

//mocks for test
import currentMock from '../reducers/mock_current'
import modifiedJson from '../reducers/mock_modified'

import styled from 'styled-components'

import axios from 'axios'

import {Speech} from '../components/speech';

const Grid = styled.div`
  .container{
    display:flex;

    input:not(.chatMessageInput){
      width: 100%;
      height: 40px;
      font-size: 20px;
    }

    button{
      display: block;
      width: 140px;
      height: 44px;
      border-radius: 3px;
      border-bottom: none;
      background: linear-gradient(135deg, #c82273 0%, #e24a35 100%);
      color: #fff;
      font-family: oiTextRegular, sans-serif;
      font-size: 1.125rem;
      text-align: center;
      text-decoration: none;
      transition: background-color 233ms linear !important;
    }

    .chatMessageInput{
      opacity: 0;
      height: 0;
      width: 0;
      position: absolute;
      left: -999999999;
    }
  }

  .loader-container{
    height: 100px;

    .loader{

    }
  }


  .list{
    display: flex;
    flex-direction: column;

    .container-response{
      padding: 19px;
      background-image: white;
      border-bottom: 1px solid black;
      transition: background-color 300ms,box-shadow 300ms;

      /*:hover{
        opacity: 0.7;
      }*/

      .ask{
        background-image: -webkit-linear-gradient(left,#de3267,#f8562c);
        font-weight: bolder;
        padding: 15px;
        color: white;
      }

      .answer{
        min-height: 30px;
        padding: 15px;
      }
    }
  }
`

const listSearched = (state) =>{
  let {result} = state;
  if(result && Array.isArray(result) && result.length > 0){
    return result.map((item, index)=>{
      return (
        <li key={`__container-response-${index}`} className="container-response">
          <div className="ask">
            Pergunta: {item.ask}
          </div>
          <div className="answer">
            Resposta: {item.answer}
          </div>
        </li>
      )
    })
  }

  return null;
}

class AsyncContainer extends Component {
  constructor(props){
    super(props)

    this.state = {
      result: [] ,
      current: '',
      init: false,
      loading: false
    }

    this.sendRequest = this.sendRequest.bind(this)
    this._Speech = this._Speech.bind(this)
  }

  sendRequest = (search) =>{
    let _self = this;
    this.setState({loading:true})

    setTimeout(()=>{
      axios.get(`http://localhost:3030/api/v1/search/term/${search}`)
      .then(response => {
        console.log(response)
        const result = response.data.hits.hits.map(obj => obj._source);
        _self.setState({ result, current: '', init: true, loading: false});
      });
    }, 1500)

  }

  _SetCurrentTextField(text){
    this.setState({current:text})
  }

  _Speech(){
    return (
      <Speech
        className="speech"
        lang={'pt-BR'}
        onChange={(textSegment) => this._SetCurrentTextField(textSegment)}
        onEnd={
          (interpretedText) => {
            this.setState({current:interpretedText})
            this.sendRequest(interpretedText)
          } }/>
    )
  }

  _handlePress(event){
    if(event.key.toLowerCase() === 'enter' ){
      console.log('enter')
      this.sendRequest(event.target.value)
    }
  }

  _handleChange(event){
    this.setState({current: event.target.value})
  }

  render() {
    let {current, loading} = this.state;

    return (
      <div>
        <Grid {...this.props}>
          <div className="container">

            {/* the input component */}
            <input
              value={current}
              onKeyPress={(event) => this._handlePress(event)}
              onChange={(event) => this._handleChange(event)}
              type="text" />

            {/* the send button */}
            <button onClick={() => this.sendRequest(current)}>Pesquisar</button>

            {/* the speech component */}
            {this._Speech()}
          </div>
          <div className="loader-container">
            {loading && <img src='/assets/images/loader.gif' className="loader"/>}
          </div>
          <ol className="list">{listSearched(this.state)}</ol>
        </Grid>
      </div>
    )
  }
}


function mapStateToProps (state) {
  //return { JSONTarget: state.PageBoilerplateReducer.get('JSONTarget') }
  return {}
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch)
}

export { AsyncContainer }
export default connect(mapStateToProps, mapDispatchToProps)(AsyncContainer)
