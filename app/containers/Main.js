import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as actions from '../actions/questions'

//mocks for test
import currentMock from '../reducers/mock_current'
import modifiedJson from '../reducers/mock_modified'

import { ThemeProvider } from 'styled-components'
import StarWars from '../components/config/themes/StarWars'

import axios from 'axios'

import Main from '../components/pages/main'

class AsyncContainer extends Component {
  render() {
    return (
      <ThemeProvider theme={ StarWars }>
        <Main/>
      </ThemeProvider>
    )
  }
}


function mapStateToProps (state) {
  //return { JSONTarget: state.PageBoilerplateReducer.get('JSONTarget') }
  return {}
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch)
}

export { AsyncContainer }
export default connect(mapStateToProps, mapDispatchToProps)(AsyncContainer)
