import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { prop, ifProp, switchProp } from 'styled-tools'

import Text from './../Text'
import * as m from './../../styles/mixins'

//async components
import Async from 'react-code-splitting'


//Component Styles
const styles = css`
    display: inline-block;
    color: #222222;
    margin: 0.1em;
    box-sizing: border-box;

    & > svg {
        height: 100%;
    }
`

const StyledIcon = styled.i`
    ${styles}
    font-size: ${ props => props.size && `${m.calcSize(props.size)}` };
    width: ${ props => props.width ? ` ${rem(props.width)}` :  `${m.calcSize(props.size)}`};
    height: ${ props => props.height ? ` ${rem(props.height)}` : `${m.calcSize(props.size)}`};
    ${ifProp('autoSize', css`width: auto;`)}
    & svg  :not([fill="none"]), :not([fill="#ffffff"]) {
        fill:  ${prop('fillColor')};
        stroke:  ${ifProp("strokeColor", prop('strokeColor'))};
        .nofill{
            fill: #FFFFFF;
        }
        ${switchProp('themeColor', {
            primary: css`fill: ${prop('theme.colors.primary')};`,
            secondary: css`fill: ${prop('theme.colors.secondary')};`,
            success: css`fill: ${prop('theme.colors.success')};`,
            error: css`fill: ${prop('theme.colors.error')};`,
            warning: css`fill: ${prop('theme.colors.warning')};`,
            info: css`fill: ${prop('theme.colors.info')};`,
            light: css`fill: ${prop('theme.colors.light')};`,
            dark: css`fill: ${prop('theme.colors.dark')};`,
            link: css`fill: ${prop('theme.colors.link')};`
        })}

    }
`

const getIcon = (props) => {
    //return Object
            //.getOwnPropertyNames(objects)
    //return icons[props.type] ? icons[props.type](props) : (<svg></svg>);
    //return <Async load={import('./iconfiles/' + props.type)} componentProps={props}/>
    //
    //
    return <i></i> //removed async import 
}

//Component Core
const Icon = (props) => (
    <StyledIcon {...props}>
            {getIcon(props)}
    </StyledIcon>
)

//Component Props
Icon.PropTypes = {
    themeColor: PropTypes.oneOf(['primary', 'secondary', 'success', 'error', 'warning', 'info', 'light', 'dark', 'link']),
    type: PropTypes.string.isRequired,
    size: PropTypes.oneOf([1, 2, 3, 4, 5, 6, 7]),
    width: PropTypes.number,
    height: PropTypes.number,
    autoSize: PropTypes.bool,
    fillColor: PropTypes.string,
    strokeColor: PropTypes.string,
}

Icon.defaultProps = {
    size: 2,
    fillColor: "#222222",
}

export default Icon
export {Icon}
