const Pink = {
    body: {

        margin: '0',
        padding: '0',
        font: {
            family: 'Simplon',
            weight: 'Regular'
        }
    },
    colors: {
        primary: '#D82482',
        secondary: '#9F2AFF',
        success: '#00D213',
        error: '#F8562C',
        warning: '#FFD700',
        info: '#00BAF7',
        light: '#F5F5F5',
        dark: '#909090',
        link: '#DBDBDB',
        white: '#FFFFFF',
        black: '#222222'
    },
    gradients: [
        {
            direction: 'to left',
            colors: ['#D82482', '#00D213', '#900AE9']
        },
        {
            direction: '110deg',
            colors: ['#00BAF7', '#FFD700']
        }
    ]

}
  export default Pink
  export { Pink }
