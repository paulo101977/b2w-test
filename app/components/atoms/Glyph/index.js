import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { prop, ifProp, switchProp } from 'styled-tools'

import * as m from '../../styles/mixins'

//Component Styles
const StyledI = styled.i`
${switchProp('themeColor', {
    primary: css`color: ${prop('theme.colors.primary')};`,
    secondary: css`color: ${prop('theme.colors.secondary')};`,
    success: css`color: ${prop('theme.colors.success')};`,
    error: css`color: ${prop('theme.colors.error')};`,
    warning: css`color: ${prop('theme.colors.warning')};`,
    info: css`color: ${prop('theme.colors.info')};`,
    light: css`color: ${prop('theme.colors.light')};`,
    dark: css`color: ${prop('theme.colors.dark')};`,
    link: css`color: ${prop('theme.colors.link')};`
})}
`

//Component Core
const Glyph = (props) => (
    <StyledI
      {...props}>
        {/* render the children */}
        {props.children}
    </StyledI>
);



//Component Props
Glyph.propTypes = {
    themeColor: PropTypes.oneOf(['primary', 'secondary', 'success', 'error', 'warning', 'info', 'light', 'dark', 'link']),
    color: PropTypes.string
}

Glyph.defaultProps = {
    color: '#fff'
};

export { Glyph }
export default Glyph;
