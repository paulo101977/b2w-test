import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import axios from 'axios'

import * as actions from '../actions/questions' 

import SearchSpeech from '../components/organisms/SearchSpeech'
import TitleDescription from '../components/molecules/TitleDescription'
import { Row, Col } from  '../components/atoms/container'

class Test extends Component {
  constructor(props){
    super(props)
    this.state = { data: [], loadding: false}
    this._onEnd = this._onEnd.bind(this)
  }

  _onEnd(search){
    console.log('onEnd',search)
    this.setState({loadding:true})

    setTimeout(()=>{
      axios.get(`http://localhost:3030/api/v1/search/term/${search}`)
      .then(response => {
        console.log(response)
        const result = response.data.hits.hits.map(obj => obj._source);
        this.setState({ data: result,loadding: false});
        console.log(result)
      });
    }, 1500)
  }

  componentDidMount(){
  /*  axios.get(`http://localhost:3030/api/v1/search/term/segunda via`)
      .then(response => {
        console.log(response)
        const result = response.data.hits.hits.map(obj => obj._source);
        this.setState({ data: result,loadding: false});
        console.log(result)
      }); */
  }

  render() {

    return (
      <Row spaced sizeFull>
        <Col sizeFull>
          <Col >
            Engine de buscas em 2008
          </Col>
          <Col >
            <SearchSpeech onEnd={this._onEnd} userEpeech={true}/>
          </Col>
          <Col >
            {this.state.loadding && <img src='/assets/images/loader.gif' />}
          </Col>
         
            { 
              this.state.data.map((row, key) => {
                console.log(row)
                return ( <Col  key={key} >
                         {/* <p>alt_ask: <em>{row.alt_ask}</em></p>
                          <p>Keywords: <em>{row.keywords}</em></p>*/}
                          <TitleDescription 
                             line 
                                Title2={row.ask} 
                                Description2={row.answer} />
                          </Col>)
            })
          }
        

        </Col>
      </Row>
    )
  }
}


function mapStateToProps (state) {
  //return { JSONTarget: state.PageBoilerplateReducer.get('JSONTarget') }
  return {}
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch)
}

export { Test }
export default connect(mapStateToProps, mapDispatchToProps)(Test)
