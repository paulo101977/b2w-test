// Importar bibliotecas do react.
import React from 'react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import { ThemeProvider } from 'styled-components'
import Pink from '../../config/themes/Pink'


import Card from '.';

storiesOf('Organisms/Card', module)
.add('Uso Simples',
withInfo({
    text: 'Rótulos ou contadores de notificações',
})(() =>

  <ThemeProvider theme={ Pink }>
    <Card />
  </ThemeProvider>

))
