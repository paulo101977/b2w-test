import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { prop, ifProp, switchProp } from 'styled-tools'

import Grid from './../../atoms/Grid'
import Button from './../../atoms/Button'
import List from './../../atoms/List'
import Text from './../../atoms/Text'
import Title from './../../atoms/Title'
import Icon from './../../atoms/Icon'
import * as m from '../../styles/mixins'
import Card from '../../organisms/Card'

import axios from 'axios'

const getColor = (props, color) => props.theme.colors.primary

const MainWrapper = styled.div`
  transition: 500ms -webkit-filter linear;
  filter: blur(0);

  &.loading{
    filter: blur(5px);
  }

  .main-container{
    display: flex;
    flex-direction: column;
    position: relative;

    .card-wrapper{
      position: relative;
      z-index: 1000001;
    }

    .button-wrapper{
      position: relative;
      z-index: 1;
      padding: 0 1rem;

      button{
        background-color: white !important;
        color: ${props => getColor(props)};
        border-color: ${props => getColor(props)};
        transition: transform 300ms;

        :hover{
          transform: scale(1.1);
        }
      }
    }
  }
`

//Component Props
class Main extends Component {
  constructor(props){
    super(props)

    this.state = {
      results: [] ,
      current: null,
      init: false,
      loading: false,
      selected: -1
    }

    this.sendRequest = this.sendRequest.bind(this)
    this.getNextPlanet = this.getNextPlanet.bind(this)
    this.goToNext = this.goToNext.bind(this)
  }

  sendRequest = () =>{
    let _self = this;
    this.setState({loading:true})

    //simulate a fake loading
    setTimeout(()=>{
      axios.get(`https://swapi.co/api/planets/`)
      .then(response => {
        const results = response.data.results;
        _self.setState({
          results: results,
          current: null,
          init: true,
          loading: false
        });
      });
    }, 1500)

  }

  componentDidMount(){
    this.sendRequest()
  }

  componentWillUpdate(nextProps, nextState){
    if(this.state !== nextState){
      return true;
    }

    return false;
  }

  _handleClick(){
    console.log('get the next')
  }



    renderCard(){
      const {results} = this.state;
      const next = this.getNextPlanet()
      if(!results.length){
        return <Card />
      } else {
        return (
          <Card
            {...next} />
        )
      }
    }

    getNextPlanet(){
      let results = this.state.results;
      let length = results.length;
      let rndInt = parseInt(Math.random()*length)
      let nextResult = results[rndInt]

      //remove one element
      //results.splice(rndInt, 1);
      // this.setState({current: nextResult})

      return nextResult;
    }

    goToNext(){
      this.setState({
        init: !this.state.init,
        loading: true
      })

      setTimeout(()=>{
        this.setState({loading: false})
      },400)
    }


    render() {
        const {loading} = this.state;
        const loadingCls = loading ? 'loading' : '';

        return(
          <MainWrapper className={`${loadingCls}`} >
            <Grid className="main-container" col align="center">
              <Grid className="card-wrapper col-md-6">
                {this.renderCard()}
              </Grid>
              <Grid className="button-wrapper col-md-6">
                <Button
                  disabled={loading}
                  onClick={this.goToNext}
                  themeColor="primary"
                  type="outline">Next</Button>
              </Grid>
            </Grid>
          </MainWrapper>
        )
    }

}

Main.PropTypes = {
    selected: React.PropTypes.number,
    children: React.PropTypes.oneOfType([
      React.PropTypes.array,
      React.PropTypes.element
    ]).isRequired
  }

Main.defaultProps = {
    selected: 0
}

export {Main}
export default Main
