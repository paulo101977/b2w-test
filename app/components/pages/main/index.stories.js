// Importar bibliotecas do react.
import React from 'react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import { ThemeProvider } from 'styled-components'
import StarWars from '../../config/themes/StarWars'

import MainPage from '.'


storiesOf('Pages/Main', module)
.add('Uso Simples',
withInfo({
    text: 'Rótulos ou contadores de notificações',
})(() =>

  <ThemeProvider theme={ StarWars }>
    <MainPage />
  </ThemeProvider>

))
